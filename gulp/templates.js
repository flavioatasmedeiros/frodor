var _ = require('lodash');
var gulp = require('gulp');
var nujucks = require('gulp-nunjucks-render');
var gutil = require('gulp-util')
var wrapper = require('gulp-wrapper');
var sections = require('../app/scripts/config').sections;

module.exports = _.curry(templates, 2);

function templates(templateDir, buildDir) {
  return function() {
    pages(templateDir, buildDir);
    macros(templateDir, buildDir);
  }
}

function pages(templateDir, buildDir) {
  gulp.src(templateDir + '/pages/**/*.html')
    .pipe(wrapper({
      header: '{% extends "' + templateDir +  '/layout.html" %}{% block content %}{% raw %}',
      footer: '{% endraw %}{% endblock %}'
    }))
    .pipe(nujucks({sections: sections}))
    .pipe(gulp.dest(buildDir))
    .on('end', function() {
      gutil.log('Page templates created')
    });
}

function macros(templateDir, buildDir) {
  gulp.src(templateDir + '/macros/**/*.html')
    .pipe(gulp.dest(buildDir + '/macros'))
    .on('end', function() {
      gutil.log('Import templates created')
    });
}
