var _ = require('lodash');
var browserify = require('browserify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var gulp = require('gulp');
var gutil = require('gulp-util');

var error = require('./error');

module.exports = _.curry(bundleScripts, 4);

// Based on: http://blog.avisi.nl/2014/04/25/how-to-keep-a-fast-build-with-browserify-and-reactjs/
function bundleScripts(scriptsDir, buildDir, file, watch) {

  var props = {entries: [scriptsDir + '/' + file]};
  var bundler = watch ? watchify(props) : browserify(props);

  function rebundle() {
    
    var stream = bundler.bundle({debug: true});
    
    return stream.on('error', error)
      .pipe(source(file))
      .pipe(gulp.dest(buildDir + '/'))
      .on('end', function() {
        gutil.log('Browserify bundled.');  
      });
  }

  bundler.on('update', function() {
    rebundle();
    gutil.log('Rebundling...');  
  });

  return rebundle();
}
