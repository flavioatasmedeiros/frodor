var model = require('./lib/model');
var adapter = require('./adapter');

module.exports = {
  faq: model.extend(adapter('faq')),
  funcionalidade: model.extend(adapter('funcionalidade')),
};

