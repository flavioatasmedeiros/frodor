var $ = require('jquery-browserify');

var standardForm = function(name) {
  var id = app.query.id;
  app.menu(name).models[name].find(id).then(function(model) {
    app.render($('#template'), model);
    app.form($('#form'), model, function() { 
      window.location.href = '/' + name + '/listar.html'; 
    });
  });
};


var standardList = function(name) {
  app.menu(name).models[name].all().then(function(model) {
    app.render($('#template'), model);
  });
};


module.exports = {
  form: standardForm,
  list: standardList
};

