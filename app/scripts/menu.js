var $ = require('jquery-browserify');

module.exports = function(section) {

  var $menu = $('[role=navigation]');

  $menu.find('li').removeClass('active');
  
  $menu.find('[data-section=' + section + ']').addClass('active');

  return this;
};

