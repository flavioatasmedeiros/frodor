var qs = require('querystring');

module.exports = function($form, model) {

  model.data = qs.parse($form.serialize());

  return model;
};
