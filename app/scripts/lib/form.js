var _ = require('lodash');
var formToModel = require('./form-to-model');


module.exports = function($form, model, callback) {

  var isSending = false;
  var $submitButton = $form.find('[type=submit]');


  $form.on('submit', function(ev) {
    ev.preventDefault();
    handleSubmit();
  });


  function handleSubmit() {
    
    if (isSending) { return; }
    isSending = true;
    $submitButton.addClass('disabled');

    formToModel($form, model);
    var promise = model.save();
 
    promise.then(function() { 
      isSending = false;
      $submitButton.removeClass('disabled');
    });

    promise.then(callback);
  }

};
