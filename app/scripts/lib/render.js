var _ = require('lodash');

var replaceParent = require('./replace-parent');
var generateHtml = require('./generate-html');

module.exports = function($template, model) {
  
  var html = generateHtml($template.html(), model.data);

  var parent = replaceParent($template[0], html);

  return parent;
};
