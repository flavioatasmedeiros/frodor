var _ = require('lodash');


var setData = function(response) {
  this.data = response;
  return this;
};


module.exports = {
  
  extend: function(adapter, props) {
    return _.extend({}, props||{}, {adapter: adapter}, this);
  },

  all: function(query) {
    return this.adapter.all(query||{})
      .pipe(setData.bind(this));
  },

  find: function(id) {
    return this.adapter.find({id: id})
      .pipe(setData.bind(this));
  },

  save: function() {
    return this.adapter.save(this.data);
  },

  delete: function(id) {
    return this.adapter.delete({id: id})
  }

};

 