var _ = require('lodash');

module.exports = _.curry(replaceParent, 2);

function replaceParent(element, html) {

  var parent = element.parentNode;

  parent.innerHTML = html;
  
  return parent;
}