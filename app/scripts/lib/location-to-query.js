var querystring = require('querystring');

var query = window.location.search.replace('?', '');

var parsed = querystring.parse(query);

module.exports = parsed;
