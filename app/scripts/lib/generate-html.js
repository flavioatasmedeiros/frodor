var _ = require('lodash');
var nunjucks = require('nunjucks');

module.exports = _.curry(generate, 2);

function generate(template, data) {
  
  var html = nunjucks.renderString(template, data);

  return html;
}
