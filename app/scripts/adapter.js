var _ = require('lodash');
var $ = require('jquery-browserify');
var config = require('./config');


module.exports = function(entity) {
  var root = config.api.root;
  return {
    all:    _.partial(request, root, entity, 'listar'),
    save:   _.partial(request, root, entity, 'salvar'),
    find:   _.partial(request, root, entity, 'obter'),
    delete: _.partial(request, root, entity, 'deletar')
  };
};


function request(root, entity, cmd, query) {
  var url = [ root, entity, cmd ].join('/');
  var promise = $.ajax({
    dataType: 'json', // Forçando, já que o backend e/ou proxy não está mandando o header correto. Verificar.
    url: url,
    data: query||{},
  });
  handleError(promise);
  handleFetch(promise);
  return promise;
}


function handleError(promise) {
  promise.fail(function() {
    console.error(arguments);
  });
  return promise;
}


function handleFetch(promise) {
  handleError(promise);
  promise.then(function(response) {
    if (response.errors) {
      alert( _.pluck(response.errors, 'message').join('\n') );
      return;
    }
    if (! response.item && ! response.success) { // Entender porque não está vindo response.success sempre
      alert('Erro não identificado.');
    }
  });
  return promise;
}

