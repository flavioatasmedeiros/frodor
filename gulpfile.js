var gulp = require('gulp');
var browserSync = require('browser-sync');


var scripts = require
  ('./gulp/scripts.js')
  ('./app/scripts')
  ('./__build/scripts')
  ('./main.js');


var templates = require
  ('./gulp/templates')
  ('./app/templates')
  ('./__build');


gulp.task('default', function() {
  scripts(false); // Do not watch.
  scripts(true); // Watch!
  templates();
  gulp.watch('./app/templates/**/*.*', templates);
  browserSync.init(['./build/**/*.*'], {
    server: {baseDir:  './__build'},
    directory: true
  });
});


gulp.task('scripts', function() {
  scripts(false); // Do not watch.
});


gulp.task('watch-scripts', function() {
  scripts(true); // Watch!
});


gulp.task('templates', function() {
  templates();
});


gulp.task('watch-templates', function() {
  gulp.watch('./app/templates/**/*.*', templates);
});

